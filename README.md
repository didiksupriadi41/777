# randomslot
This project was created to meet a completion of SPARTA2017 work assignment.

- WINDOWS: [winrandomslotapp-0.1.0.zip](https://github.com/didiksupriadi41/randomslot/releases/download/v0.1.0/winrandomslotapp-0.1.0.zip)
- Linux: [linuxrandomslotapp-0.1.0.zip](https://github.com/didiksupriadi41/randomslot/releases/download/v0.1.0/linuxrandomslotapp-0.1.0.zip)

#### Requirements:
  - Java

#### How to use:
  - For WINDOWS, Open folder 'bin/' and execute 'RandomSlotApp.bat'
  - For Linux, Open folder 'bin/' in terminal and execute './RandomSlotApp'

#### NOTE for WINDOWS User:
  It is possible to see a different result when executing on WINDOWS machine.
  The reason is WINDOWS's 'Command prompt' do not have maximum range of character value.
  (it means, you would see '?' instead of the actual Unicode character)
